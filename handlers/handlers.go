package handlers

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"regexp"

	"github.com/oscarpfernandez/rest-sms-api/dispatcher"
	"github.com/oscarpfernandez/rest-sms-api/encoding"
	"github.com/oscarpfernandez/rest-sms-api/sms"
)

var (
	ErrExpectedPostMethod = errors.New("Invalid method. POST expected")
	ErrInvalidOriginator  = errors.New("Originator is empty or invalid")
	ErrInvalidRecipient   = errors.New("Recipient is empty or invalid")
	ErrMessageIsEmpty     = errors.New("Message is empty")
	ErrMessageIsToLarge   = errors.New("Message payload is too large")
	ErrMessageUnmarshall  = errors.New("Could not unmarshall request message")
	ErrRequestBody        = errors.New("Could not read request body")
)

const (
	MBMaxNumSms = 9
)

// SmsRequest the object containg the required fields to deliver
// an SMS message to the gateway.
type SmsRequest struct {
	Recipient  string `json:"recipient"`
	Originator string `json:"originator"`
	Message    string `json:"message"`
}

type SmsResponse struct {
	Response string `json:"response"`
}

type ErrorResponse struct {
	Error string `json:"error"`
}

// SendSMSPOST receives an SMS message request
func SendSMSPOST(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		handleError(w, ErrExpectedPostMethod, http.StatusBadRequest)
		return
	}

	buffer, err := ioutil.ReadAll(r.Body)
	if err != nil {
		handleError(w, ErrRequestBody, http.StatusInternalServerError)
		return
	}

	smsReq := &SmsRequest{}
	err = json.Unmarshal(buffer, smsReq)
	if err != nil {
		handleError(w, ErrMessageUnmarshall, http.StatusBadRequest)
		return
	}

	errDesc := smsReq.validate()
	if errDesc != nil {
		handleError(w, errDesc, http.StatusBadRequest)
		return
	}

	msg := &sms.Message{
		Originator: smsReq.Originator,
		FullText:   smsReq.Message,
		Recipient:  smsReq.Recipient,
		Encoding:   "utf8",
	}
	dispatcher.PushMessageToQueue(msg)

	w.WriteHeader(http.StatusAccepted)
	resp := &SmsResponse{
		Response: "Your message was accepted and queued for processing.",
	}
	jsonResp, _ := json.MarshalIndent(resp, "", "  ")
	w.Write(jsonResp)
}

func handleError(w http.ResponseWriter, err error, statusCode int) {
	errResp := &ErrorResponse{
		Error: err.Error(),
	}
	errJson, _ := json.Marshal(errResp)
	http.Error(w, string(errJson), statusCode)
}

// validate verifies the message request's content
func (dm *SmsRequest) validate() error {
	if len(dm.Originator) == 0 || len(dm.Originator) > 11 {
		return ErrInvalidOriginator
	}
	if len(dm.Recipient) == 0 || !isNumber(dm.Recipient) {
		return ErrInvalidRecipient
	}
	if len(dm.Message) == 0 {
		return ErrMessageIsEmpty
	}

	msgBytes := encoding.UTF8ToGSM(dm.Message)
	if len(msgBytes) > MBMaxNumSms*(sms.SmsMaxPayloadBytes) {
		return ErrMessageIsToLarge
	}

	return nil
}

// isNumber verifies if the string represents a number
func isNumber(n string) bool {
	isNumber, _ := regexp.MatchString("^[0-9]+$", n)
	return isNumber
}
