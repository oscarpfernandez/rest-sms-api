package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/oscarpfernandez/rest-sms-api/dispatcher"
	"github.com/oscarpfernandez/rest-sms-api/handlers"
)

func main() {

	// Parse command line args
	var mbAPIKey string
	flag.StringVar(&mbAPIKey, "mb-api-key", "", "MessageBird access API Key")
	reqsPerSecond := flag.Uint("mb-max-rps", 1, "Maximum requests per second to MessageBird")

	flag.Parse()

	if len(os.Args) == 0 || len(mbAPIKey) == 0 || *reqsPerSecond == 0 {
		flag.PrintDefaults()
		os.Exit(1)
	}

	go dispatcher.WorkerCreateConcatSms(*reqsPerSecond)
	go dispatcher.WorkerLogProcessingResp()
	go dispatcher.WorkerSendSmsToMB(mbAPIKey)

	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		dispatcher.CloseResources()
		log.Print("Shutting down server.")
		os.Exit(1)
	}()

	http.HandleFunc("/api/send-sms", handlers.SendSMSPOST)

	log.Print("Starting SMS server")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
