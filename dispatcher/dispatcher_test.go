package dispatcher

import (
	"testing"
	"time"

	"github.com/oscarpfernandez/rest-sms-api/sms"
)

func TestPushMessage(t *testing.T) {
	msg := &sms.Message{
		FullText:   "This is just a sample message",
		Originator: "MessageBird",
		Recipient:  "12345",
		Encoding:   "utf8",
	}

	go WorkerCreateConcatSms(10)

	PushMessageToQueue(msg)

	timeout := make(chan bool, 1)
	go func() {
		time.Sleep(1000 * time.Millisecond)
		timeout <- true
	}()

	select {
	case <-mbSmsDispatcherChan:
		//message sent correctly to pipeline
	case <-timeout:
		t.Fatal("Message not received on time")
	}
}

func TestPushMessageSequence(t *testing.T) {
	msg := &sms.Message{
		FullText:   "This is just a sample message",
		Originator: "MessageBird",
		Recipient:  "12345",
		Encoding:   "utf8",
	}

	go WorkerCreateConcatSms(100)

	expNumMsgs := 50
	for i := 0; i < expNumMsgs; i++ {
		PushMessageToQueue(msg)
	}

	timeout := make(chan bool, 1)
	go func() {
		time.Sleep(1500 * time.Millisecond)
		timeout <- true
	}()

	gotNumMsgs := 0
	for {
		select {
		case <-mbSmsDispatcherChan:
			gotNumMsgs++
		case <-timeout:
			if expNumMsgs != gotNumMsgs {
				t.Fatalf("Messages not sent correctly before timeout. exp: %d, got: %d", expNumMsgs, gotNumMsgs)
			}
			return
		}
	}
}
