package dispatcher

import (
	"log"
	"time"

	messagebird "github.com/messagebird/go-rest-api"
	"github.com/oscarpfernandez/rest-sms-api/mbapi"
	"github.com/oscarpfernandez/rest-sms-api/sms"
)

const (
	DefaultChannelLen      = 150
	mbSmsDispatcherChanLen = 4 * DefaultChannelLen
)

var (
	messageReqChan      = make(chan *sms.Message, DefaultChannelLen)
	mbSmsDispatcherChan = make(chan *sms.SmsBlock, mbSmsDispatcherChanLen)
	mbResponseChan      = make(chan *messagebird.Message, DefaultChannelLen)
	processingErrChan   = make(chan *ProcessingError, DefaultChannelLen)
)

// ProcessingError represents a processing error, that occured
// along the worker's pipeline execution.
type ProcessingError struct {
	Stage string
	Error error
}

// PushMessageToQueue receives a sms message request, and submits
// it to the starting point of the processing pipeline.
func PushMessageToQueue(msgReq *sms.Message) {
	messageReqChan <- msgReq
}

// WorkerCreateConcatSms is a worker responsible to receive Messages,
// assemble the concatenated sms object, and finally forwards it to
// the worker responsible for submitting the sms to the gateway.
func WorkerCreateConcatSms(reqsPerSecond uint) {
	for {
		select {
		case msg := <-messageReqChan:
			sms, err := msg.CreateConcatenatedSMS()
			if err != nil {
				pErr := &ProcessingError{
					Stage: "CreateConcatenatesSMS",
					Error: err,
				}
				processingErrChan <- pErr
				continue
			}

			// rate limits the number of SMS that could be sent
			// to MessageBird's gateway.
			throttle := time.Tick(time.Duration(1000*1000/reqsPerSecond) * time.Microsecond)

			for _, smsBlock := range sms.SMSBlocks {
				// smsBlockStr, _ := json.Marshal(smsBlock)
				// log.Printf("Dispatching SmsBlock: %s", smsBlockStr)
				<-throttle
				mbSmsDispatcherChan <- smsBlock
			}
		}
	}
}

// WorkerSendSmsToMB is a worker responsible to submit the
// concatenated sms to the MessageBird gateway.
func WorkerSendSmsToMB(accessKey string) {
	mbClient := mbapi.NewClient(accessKey)

	for {
		select {
		case smsBlock := <-mbSmsDispatcherChan:
			mbMsg := mbapi.NewMessage(smsBlock.Message, smsBlock.UDH, smsBlock.Recipients, smsBlock.Originator)
			err := mbMsg.Validate()
			if err != nil {
				pErr := &ProcessingError{
					Stage: "MB message validation",
					Error: err,
				}
				processingErrChan <- pErr
				continue
			}
			mbResp, err := mbClient.SendMessage(mbMsg)
			if err != nil {
				pErr := &ProcessingError{
					Stage: "MB SendMessage",
					Error: err,
				}
				processingErrChan <- pErr
				continue
			}
			mbResponseChan <- mbResp
		}
	}
}

// WorkerLogProcessingResp is a worker responsible to handle and
// log responses sent by the other pipelines.
func WorkerLogProcessingResp() {
	for {
		select {
		case pErr := <-processingErrChan:
			log.Printf("[ Error ][ Stage: %s ]: %v", pErr.Stage, pErr.Error)
		case mbResp := <-mbResponseChan:
			log.Printf("[ OK ] MB Message response: %+v", mbResp)
		}
	}
}

// CloseResources closes all the pipelines communications channels
// used by the several workers.
func CloseResources() {
	close(processingErrChan)
	close(messageReqChan)
	close(mbSmsDispatcherChan)
	close(mbResponseChan)
}
