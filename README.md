# REST-sms-api

The project implements a REST interface, to submit SMS messages to MessageBird.

## Endpoints
```
POST /api/send-sms
```
```
{
    "originator":"MessageBird", 
    "recipient":"31653596000", 
    "message":"This is the sms message"  
}

```

## Start the Server

```
./rest-sms-api -mb-api-key=<API_Key> -mb-max-rps=1 
```

## Submit a message

```
$ curl -v -X POST http://127.0.0.1:8080/api/send-sms --data '{"originator":"MessageBird", "recipient":"31653596068", "message":"The sms message"  }'
```
