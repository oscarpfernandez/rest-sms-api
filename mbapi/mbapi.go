package mbapi

import (
	"encoding/hex"
	"errors"

	"github.com/messagebird/go-rest-api"
)

const (
	messageType   = "binary"
	datacoding    = "plain"
	typeDetailUDH = "udh"
)

var (
	ErrNoText       = errors.New("Message does not have text")
	ErrNoOriginator = errors.New("Message does not have originator")
	ErrNoRecipients = errors.New("Message does not have recipients")
	ErrNoUDH        = errors.New("Message does not have UDH")
)

type Message struct {
	originator string
	text       string
	recipients []string
	msgParams  *messagebird.MessageParams
}

func NewMessage(text []byte, udh []byte, recipients []string, originator string) *Message {
	return &Message{
		text:       hex.EncodeToString(text),
		originator: originator,
		recipients: recipients,
		msgParams: &messagebird.MessageParams{
			Type:       messageType,
			DataCoding: datacoding,
			TypeDetails: messagebird.TypeDetails{
				typeDetailUDH: hex.EncodeToString(udh),
			},
		},
	}
}

// Validate verifies if the appropriate fields are present and ready
// for submission. At this point the validation is more simplified just
// to check if the parameters are set.
func (msg *Message) Validate() error {
	if len(msg.text) == 0 {
		return ErrNoText
	}
	if len(msg.recipients) == 0 {
		return ErrNoRecipients
	}
	if len(msg.originator) == 0 {
		return ErrNoOriginator
	}
	if _, exists := msg.msgParams.TypeDetails[typeDetailUDH]; !exists {
		return ErrNoUDH
	}

	return nil
}

type Client struct {
	client *messagebird.Client
}

func NewClient(accessKey string) *Client {
	return &Client{
		client: messagebird.New(accessKey),
	}
}

func (c *Client) SendMessage(msg *Message) (*messagebird.Message, error) {
	return c.client.NewMessage(msg.originator, msg.recipients, msg.text, msg.msgParams)
}
