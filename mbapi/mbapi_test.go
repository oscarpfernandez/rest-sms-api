package mbapi

import (
	"reflect"
	"testing"
)

var testSet = []struct {
	msg           *Message
	expectedError error
}{
	{
		msg:           NewMessage([]byte{0x00}, []byte{0x00}, []string{"1234"}, "originator"),
		expectedError: nil,
	},
	{
		msg:           NewMessage([]byte{}, []byte{0x00}, []string{"1234"}, "originator"),
		expectedError: ErrNoText,
	},
	{
		msg:           NewMessage([]byte{0x00}, []byte{0x00}, []string{}, "originator"),
		expectedError: ErrNoRecipients,
	},
	{
		msg:           NewMessage([]byte{0x00}, []byte{0x00}, []string{"1234"}, ""),
		expectedError: ErrNoOriginator,
	},
}

func TestMessageValidation(t *testing.T) {
	for iter, test := range testSet {
		err := test.msg.Validate()
		if exp, got := test.expectedError, err; !reflect.DeepEqual(exp, got) {
			t.Fatalf("Message validation result is invalid on iter: %d. exp: %v, got: %v",
				iter, exp, got)
		}
	}
}
